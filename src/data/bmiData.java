/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Observable;

/**
 *
 * @author Mohsin Ali
 */
public class bmiData extends Observable {

    private float weight, height, bmiResult;

    public void setWeight(float weight) {
        this.weight = weight;
        calculateBMI();
    }

    public float getWeight() {
        return weight;
    }

    public void setHeight(float height) {
        this.height = height;
        calculateBMI();
    }

    public float getHeight() {
        return height;
    }

    public void setBMIResult(float result) {
        this.bmiResult = result;
        setChanged();
        notifyObservers(this.bmiResult);
    }

    public float getBMIresult() {
        return bmiResult;
    }

    public void calculateBMI() {
        if (getHeight()==0) return;
        setBMIResult(getWeight() / (getHeight() * getHeight()));
    }

}
